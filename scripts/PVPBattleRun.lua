local apiUtils = require("SynchronyExtendedAPI.utils.APIUtils")
local entities = require("SynchronyExtendedAPI.extended.Entities")

local ecs = require("system.game.Entities")

local event = require("necro.event.Event")
local player = require("necro.game.character.Player")
local freeze = require("necro.game.character.Freeze")
local invincibility = require("necro.game.character.Invincibility")
local inventory = require("necro.game.item.Inventory")
local team = require("necro.game.character.Team")
local object = require("necro.game.object.Object")
local customEntities = require("necro.game.data.CustomEntities")
local collision = require("necro.game.tile.Collision")
local commonSpell = require("necro.game.data.spell.CommonSpell")
local move = require("necro.game.system.Move")
local marker = require("necro.game.tile.Marker")
local health = require("necro.game.character.Health")
local spell = require("necro.game.spell.Spell")
local currency = require("necro.game.item.Currency")
local objectEvents = require("necro.game.object.ObjectEvents")

local components = require("necro.game.data.Components")

components.register {
        respawnHolder = {},
        spellcastRespawn = {},
}

local function setPlayerUniqueTeam(entity)
        if entity.controllable.playerID ~= 0 then
                team.setTeam(entity, entity.controllable.playerID + 2)
        end
end

-- Modify spawning players
apiUtils.safeAddEvent(
        event.objectControllerChanged,
        "initPVPCharacters",
        {
                order = "player",
                sequence = 1,
        },
        function (ev)
                if ev.entity.controllable.playerID ~= 0 then
                        -- Add respawn item
                        inventory.add(object.spawn("PVPBATTLERUN_Ankh", ev.entity.position.x, ev.entity.position.y), ev.entity, true)

                        -- Modify team
                        setPlayerUniqueTeam(ev.entity)

                        -- Give spawn-protection (joining local coop, latejoin?)
                        spell.cast(ev.entity, "SpellcastShield")
                end
        end)

-- Give spawn-protection, execute pending respawns
apiUtils.safeAddEvent(
        event.gameStateLevel,
        "markPlayersPVP",
        "players",
        function ()
                for _, entity in ipairs(player.getPlayerEntities()) do
                        -- Give spawn-protection (level transition)
                        spell.cast(entity, "SpellcastShield")

                        local actionItems = inventory.getItemsInSlot(entity, "action")
                        for i = 1, #actionItems do
                                local item = actionItems[i]
                                if item and ecs.getEntityTypeName(item) == "PVPBATTLERUN_SpellRespawn" then
                                        spell.cast(entity, "PVPBATTLERUN_SpellcastRespawn")
                                end
                        end
                end
        end)


-- Create Ankh
customEntities.extend {
                name = "Ankh",
                template = customEntities.template.item(),
        
                data = {
                        flyaway = "Ankh",
                        hint = "Respawn after death.",
                        slot = "misc",
                },
        
                components = {
                        sprite = {
                                texture = "mods/PVPBATTLERUN/gfx/Ankh.png",
                        },
                        invincibility = {},
                        spellCooldownTime = {
                                cooldown = 20,
                        },
                        PVPBATTLERUN_respawnHolder = {
                        }
                },
        }

-- Add death event listener
apiUtils.safeAddEvent(
        event.holderTakeDamage,
        "deathTimeOut", 
        {
                order = "death",
                filter = "PVPBATTLERUN_respawnHolder"
        },
        function (ev)
                if not ev.suppressed and not ev.survived then
                        -- fake player death
                        ev.survived = true
                        objectEvents.fire("death", ev.holder, {killer = ev.attacker})

                        -- immobile / invincible on player
                        invincibility.activate(ev.holder, 9999)
                        freeze.inflict(ev.holder, 9999)
                        
                        -- drop action items and some gold
                        local dropGoldAmount = math.floor(currency.get(ev.holder, currency.Type.GOLD) / 4)
                        if dropGoldAmount > 0 then
                                currency.subtract(ev.holder, currency.Type.GOLD, dropGoldAmount)
                                currency.create(currency.Type.GOLD, ev.holder.position.x, ev.holder.position.y, dropGoldAmount)
                        end
                        inventory.dropSlot(ev.holder, "action")
                        
                        -- add respawn item
                        local respawnSpell = object.spawn("PVPBATTLERUN_SpellRespawn")
                        respawnSpell.spellCooldownTime.remainingTurns = 20
                        inventory.add(respawnSpell, ev.holder, true)
                        
                        -- disable AI targeting
                        team.setTeam(ev.holder, 2)
                        
                        -- move to spawn
                        local x, y = marker.lookUpFirst(marker.Type.SPAWN, 0, 0)
                        move.absolute(ev.holder, x, y, move.Type.NONE)
                        object.moveToNearbyVacantTile(ev.holder)
                        ev.holder.collision.mask = collision.Type.NONE
                end
        end)

-- Create Respawn spell
entities.addItem(nil, {
        name = "SpellRespawn",

        data = {
                flyaway = "Ankh",
                hint = "Respawn after death.",
                slot = "action",
        },

        components = {
                itemCastOnUse = {
                        spell = "PVPBATTLERUN_SpellcastRespawn",
                },
                sprite = {
                        texture = "mods/PVPBATTLERUN/gfx/Ankh.png",
                },
                activeItemConsumable = false,
                spellCooldownTime = {
                        cooldown = 20,
                },
        },
})

apiUtils.safeCall(
        function()
                commonSpell.registerSpell("SpellcastRespawn", {
                        spellcast = {
                        },
                        soundSpellcast = {
                        sound = "spellGeneral",
                        },
                        friendlyName = {
                        name = "Respawn",
                        },
                        PVPBATTLERUN_spellcastRespawn = {},
                })
        end)

apiUtils.safeAddEvent(
        event.spellcast,
        "spellcastRespawn",
        {
                order = "offset",
                sequence = 1,
                filter = "PVPBATTLERUN_spellcastRespawn",
        },
        function (ev)
                -- heal player
                health.heal {
                        entity = ev.caster,
                        healer = ev.entity,
                        health = 999,
                        silent = true,
                }

                -- reset invincibility / freeze / collision
                ev.caster.collision.mask = collision.Type.CHARACTER
                freeze.thaw(ev.caster)
                ev.caster.invincibility.remainingTurns = 0

                -- Give spawn invincibility
                spell.cast(ev.caster, "SpellcastShield")

                -- reset team
                setPlayerUniqueTeam(ev.caster)

                -- destroy item
                inventory.destroy(inventory.getItemInSlot(ev.caster, "action", 1))
        end)
